Utility for collecting statistics from .dex files

## Build
```shell
mkdir build && cd build
cmake ../src && make
```

## Usage
```shell
./dexstat classes.dex
```
