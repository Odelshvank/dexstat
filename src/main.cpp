#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>
#include <iostream>
#include <vector>
#include <fstream>

#include "json.hpp"
using json = nlohmann::json;

#include "dexter/slicer/reader.h"
#include "dexter/slicer/dex_ir.h"
#include "dexter/slicer/code_ir.h"
#include "dexter/slicer/control_flow_graph.h"

constexpr size_t chunk_capacity = 8192;


class MethodAnalysis {
public:
	lir::CodeIr code_ir;
	lir::ControlFlowGraph cfg;
	
	size_t number_bb;
	double average_bb;
	double variance_bb;
public:
	MethodAnalysis(ir::EncodedMethod *method, std::shared_ptr<ir::DexFile> dex_ir):
	code_ir(method, dex_ir), cfg(&code_ir, true) {
		number_bb = cfg.basic_blocks.size();
		average_bb = 0;
		variance_bb = 0;
		
		if (!number_bb) {
			return;
		}
		
		for (auto &block: cfg.basic_blocks){
			size_t instructions = bb_instructions_size(&block);
			average_bb += instructions;
			variance_bb += instructions * instructions;
			
			//std::cout << "id: " << block.id << " instructions: " << instructions << std::endl;
		}
		
		average_bb /= number_bb;
		variance_bb = (variance_bb / number_bb) - (average_bb * average_bb);
	}
	
	static size_t bb_instructions_size(lir::BasicBlock *block){
		lir::Instruction *node = block->region.first;
		size_t size = 0;
		
		while (node) {
			if (node->IsA<lir::Bytecode>()) {
				size++;
			}
						
			if (node == block->region.last){
				break;
			}
			
			node = node->next;
		}

		return size;
	}
};


void analyze_method_json(std::shared_ptr<ir::DexFile> dex_ir, ir::EncodedMethod *method, json &to){
	// Fully qualified name
	auto method_fqn = method->decl->parent->Decl()+"."+std::string(method->decl->name->c_str());
	//std::cout << method_fqn << std::endl;
	
	MethodAnalysis method_analysis(method, dex_ir);
	
	if (!method_analysis.number_bb){
		return;
	}
		
	if (!to.count(method_fqn))
		to[method_fqn] = json::array();
	
	to[method_fqn].push_back({
		{"number_bb", method_analysis.number_bb},
		{"average_bb", method_analysis.average_bb},
		{"variance_bb", method_analysis.variance_bb}
	});
}


int main(int argc, char *argv[]){
	if (argc < 2){
		fprintf(stderr, "Please, pass the file name\n");
		return 1;
	}
	
	char *in_file_name = argv[1];
	FILE *in_file = fopen(in_file_name, "rb");
	
	if (!in_file){
		fprintf(stderr, "Can't open input .dex file: %s\n", in_file_name);
		return 1;
	}
	
	fseek(in_file, 0, SEEK_END);
	
	size_t image_size = ftell(in_file);
	dex::u1 *image = new dex::u1[image_size];
	
	fseek(in_file, 0, SEEK_SET);
	fread(image, 1, image_size, in_file);
	fclose(in_file);
	
	dex::Reader dexreader(image, image_size);
	dexreader.CreateFullIr();
	
	json json_result;
	auto dex_ir = dexreader.GetIr();
	
	for (auto &cls: dex_ir->classes){
		for (auto &method: cls->virtual_methods){
			analyze_method_json(dex_ir, method, json_result);
		}
		
		for (auto &method: cls->direct_methods){
			analyze_method_json(dex_ir, method, json_result);
		}
	}
	
	std::cout << json_result.dump(4) << std::endl;
	
	delete [] image;
	return 0;
}

